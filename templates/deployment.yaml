apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "matomo.fullname" . }}
  labels:
    {{- include "matomo.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      {{- include "matomo.selectorLabels" . | nindent 6 }}
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        {{- include "matomo.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "matomo.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      initContainers:
        - name: redis-init
          image: bitnami/redis
          imagePullPolicy: Always
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          resources:
            requests:
              memory: 50Mi
          env:
            - name: MATOMO_CACHE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.redis.auth.existingSecret }}
                  key: {{ .Values.redis.auth.existingSecretPasswordKey | default "redis-password" }}
          envFrom:
            - secretRef:
                name: {{ template "matomo.fullname" . }}-env
          command:
            - /bin/bash
            - -c
          args:
            - for i in {30..0}; do redis-cli -h ${MATOMO_CACHE_HOST} -a ${MATOMO_CACHE_PASSWORD} ping &> /dev/null;
              if [[ $? == 0 ]]; then
              echo "Connection to Redis established."; exit 0; fi;
              echo "Redis init process in progress..."; sleep 1; done;
              echo "No connection to Redis could be established!"; exit 1;
        - name: mariadb-init
          image: bitnami/mariadb
          imagePullPolicy: Always
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          resources:
            requests:
              memory: 50Mi
          command:
            - /bin/bash
            - -c
          args:
            - for i in {30..0}; do mysql -h${MATOMO_DATABASE_HOST} -u${MATOMO_DATABASE_USER} -p${MATOMO_DATABASE_PASSWORD} -N -e "SELECT 1" &> /dev/null;
              if [[ $? == 0 ]]; then
              echo "Connection to MariaDB established."; exit 0; fi;
              echo "MariaDB init process in progress..."; sleep 1; done;
              echo "No connection to MariaDB could be established!"; exit 1;
          env:
            - name: MATOMO_DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.mariadb.auth.existingSecret }}
                  key: mariadb-password
          envFrom:
            - secretRef:
                name: {{ template "matomo.fullname" . }}-env
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          env:
            - name: MATOMO_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.matomo.auth.customSecretName }}
                  key: matomo-password
            - name: MATOMO_DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.mariadb.auth.existingSecret }}
                  key: mariadb-password
          {{- with .Values.matomo.env }}
            - name: MATOMO_SKIP_BOOTSTRAP
              value: "no"
            {{- range $key, $val := . }}
            - name: {{ $key }}
              value: {{ $val | quote }}
            {{- end }}
          {{- end }}
          envFrom:
            - secretRef:
                name: {{ template "matomo.fullname" . }}-env
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
            - name: https
              containerPort: 8443
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: https
              scheme: HTTPS
            initialDelaySeconds: 90
          readinessProbe:
            httpGet:
              path: /
              port: https
              scheme: HTTPS
            initialDelaySeconds: 90
          volumeMounts:
            - name: data
              mountPath: "{{ .Values.persistence.mountPath }}"
              subPath: "{{ .Values.persistence.subPath }}"
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        {{- if and .Values.persistence.enabled }}
        - name: data
          persistentVolumeClaim:
            claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "matomo.fullname" . }}{{- end }}
        {{- else }}
        - name: data
          emptyDir: {}
        {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
