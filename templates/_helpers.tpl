{{/*
Expand the name of the chart.
*/}}
{{- define "matomo.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "matomo.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Allow the release namespace to be overridden for multi-namespace deployments in combined charts
*/}}
{{- define "matomo.namespace" -}}
  {{- if .Values.namespaceOverride -}}
    {{- .Values.namespaceOverride -}}
  {{- else -}}
    {{- .Release.Namespace -}}
  {{- end -}}
{{- end -}}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "matomo.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "matomo.labels" -}}
{{ include "matomo.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "matomo.selectorLabels" -}}
app.kubernetes.io/name: {{ include "matomo.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "matomo.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "matomo.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "matomo.traefik.joinpaths" -}}
{{- $dict := dict "path" (list) -}}
{{- range .paths -}}
{{- $noop := printf "PathPrefix(`%s`)" . | append $dict.path | set $dict "path" -}}
{{- end -}}
{{- join " || " $dict.path -}}
{{- end }}

{{- define "matomo.mariadb.fullname" -}}
{{- printf "%s-mariadb" .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Return the MariaDB Secret Name
*/}}
{{- define "matomo.databaseSecretName" -}}
{{- if .Values.mariadb.enabled }}
    {{- if .Values.mariadb.auth.existingSecret -}}
        {{- printf "%s" .Values.mariadb.auth.existingSecret -}}
    {{- else -}}
        {{- printf "%s-mariadb-credentials" (include "matomo.fullname" .) -}}
    {{- end -}}
{{- end -}}
{{- end -}}

{{/*
Return the Redis Secret Name
*/}}
{{- define "matomo.cacheSecretName" -}}
{{- if .Values.redis.enabled }}
    {{- if .Values.mariadb.auth.existingSecret -}}
        {{- printf "%s" .Values.redis.auth.existingSecret -}}
    {{- else -}}
        {{- .Values.matomo.auth.customSecretName -}}
    {{- end -}}
{{- end -}}
{{- end -}}

{{/*
Return the Matomo Secret Name
*/}}
{{- define "matomo.authSecretName" -}}
{{- if .Values.matomo.auth.existingSecret -}}
    {{- printf "%s" .Values.matomo.auth.existingSecret -}}
{{- else -}}
    {{- printf "%s-credentials" (include "matomo.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Looks if there's an existing secret and reuse its matomo password. If not it generates
new password and use it.
*/}}
{{- define "matomo.password" -}}
{{- if .Values.matomo.auth.password }}
  {{- .Values.matomo.auth.password | b64enc | quote -}}
{{- else }}
  {{- $secret := (lookup "v1" "Secret" (include "matomo.namespace" .) .Values.matomo.auth.customSecretName ) -}}
  {{- if $secret -}}
    {{- index $secret "data" "matomo-password" -}}
  {{- else -}}
    {{- (randAlphaNum 10) | b64enc | quote -}}
  {{- end -}}
{{- end }}
{{- end -}}

{{/*
Looks if there's an existing secret and reuse its mariadb password. If not it generates
new password and use it.
*/}}
{{- define "mariadb.password" -}}
{{- if .Values.mariadb.auth.password }}
  {{- .Values.mariadb.auth.password | b64enc | quote -}}
{{- else }}
    {{- $secret := (lookup "v1" "Secret" (include "matomo.namespace" .) .Values.matomo.auth.customSecretName ) -}}
    {{- if $secret -}}
        {{- index $secret "data" "mariadb-password" -}}
    {{- else -}}
        {{- (randAlphaNum 10) | b64enc | quote -}}
    {{- end -}}
{{- end -}}
{{- end -}}

{{/*
Looks if there's an existing secret and reuse its mariadb root password. If not it generates
new password and use it.
*/}}
{{- define "mariadb.root-password" -}}
{{- if .Values.mariadb.auth.rootPassword }}
  {{- .Values.mariadb.auth.rootPassword | b64enc | quote -}}
{{- else }}
  {{- $secret := (lookup "v1" "Secret" (include "matomo.namespace" .) .Values.matomo.auth.customSecretName ) -}}
  {{- if $secret -}}
    {{- index $secret "data" "mariadb-root-password" -}}
  {{- else -}}
    {{- (randAlphaNum 10) | b64enc | quote -}}
  {{- end -}}
{{- end }}
{{- end -}}

{{/*
Looks if there's an existing secret and reuse its redis password. If not it generates
new password and use it.
*/}}
{{- define "redis.password" -}}
  {{- if .Values.redis.global.redis.password }}
  {{- .Values.redis.global.redis.password | b64enc | quote -}}
  {{- else if .Values.redis.auth.password }}
  {{- .Values.redis.global.redis.password | b64enc | quote -}}
  {{- else }}
  {{- $secret := (lookup "v1" "Secret" (include "matomo.namespace" .) .Values.matomo.auth.customSecretName ) -}}
  {{- if $secret -}}
    {{- index $secret "data" "redis-password" -}}
  {{- else -}}
    {{- (randAlphaNum 10) | b64enc | quote -}}
  {{- end -}}
  {{- end }}
{{- end -}}